package com.baltictalents.implementations;

//import com.baltictalents.lessons.Lesson9;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Task8 {

    public Integer radom () {
       Random rand = new Random();
        return rand.nextInt(100);
   }

    List<Integer> listOfNumbers = new ArrayList<>();
   public void  creatList() {

        listOfNumbers = Stream
               .iterate(0, i -> 0)
               .limit(100)
               .collect(Collectors.toList());
   }

   public void fillList(int x){
       for( int i = 0; i < x; i++){
           int random = radom();
           int oldValue = listOfNumbers.get(random);
           listOfNumbers.set(random, oldValue + 1);
       }
   }
   public void printList(int x){
       for( Integer i: listOfNumbers)
           System.out.println(i);
   }

   public void threadsgenerator() {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++)
        executor.submit(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Hello " + threadName);
            fillList(100);
        });
   }

    public void futuresgenerator() throws Exception {

       ExecutorService executor = Executors.newFixedThreadPool(5);

       ArrayList futureObjects = new ArrayList();

        Runnable task = () -> {
            fillList(100);
        };

        List<Runnable> runnables = new ArrayList<>();
        for (int i = 0; i < 5; i++)
            runnables.add(task);

        List<Future> futures = new ArrayList<>();

        for(Runnable callable : runnables) {
            futures.add(executor.submit(callable));
        }

        for(int i = 0; i < 5; i++) {
            futures.get(i).get();
        }
        executor.shutdown();
    }
}