package com.baltictalents.implementations;

import java.util.Scanner;
public class Task1 {
    public void menu() {

        Scanner scanner = new Scanner(System.in);

        byte exit = 0;

            System.out.println("");
            System.out.println("Namu darbas 2");
            System.out.println("Iseijimui is ciklo spauskyte 9");
            System.out.println("pasirinkite kuria uzduoti vykdyt (1-4): ");
            Integer u = scanner.nextInt();

            switch (u) {
                case 1:
                    task1();
                    break;
                case 2:
                    task2();
                    break;
                case 3:
                    task3();
                    break;
                case 4:
                    task4();
                    break;
                case 9:
                    exit = 1;
                    System.out.println("Baigiam darba");
                    break;
                default:
                    System.out.println("Neteisinga ivestis");
        }
    }


    // programa papraso ivesti skaiciu, i ekrana reikia ivesti ar skaicius lyginis ar nelyginis
    public static void task1() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Tikrinam skaiciu ar lyginis ");
        System.out.println("Iveskite skaiciu a = ");
        Double a = scanner.nextDouble();
        if (a % 2 == 0) {
            System.out.println("lyginis");
        } else {
            System.out.println("nelyginis");
        }
    }

    // programa papraso ivesti skaiciu, i ekrana isvedama skaiciaus saknis
    static void task2() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Saknies traukimas ");
        System.out.println("Iveskite teigiama skaiciu a = ");
        Double a = scanner.nextDouble();
        System.out.println( a > 0 ? "Saknis yra: "+ Math.sqrt(a) : "Skaiciuns turi buti teigiamas");
    }
    // programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
    // i ekrana isvedama kvadratines lygties sprendimas x1 = ?, x2 = ?
    public static void task3() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("kvadratines lygties sprendimas ax*x+bx+c");
        System.out.println("Iveskite skaiciu a = ");
        Double a = scanner.nextDouble();
        System.out.println("Iveskite skaiciu b = ");
        Double b = scanner.nextDouble();
        System.out.println("Iveskite skaiciu c = ");
        Double c = scanner.nextDouble();
        Double d = (b*b)-(4*a*c);
        if (d<0){
            System.out.println("Lygtis sprendiniu neturi");
        }else{
            Double x1 = ((b*(-1))-Math.sqrt(d))/(2*a);
            System.out.print("x1: "+"\t"+ x1+"\t");
            Double x2 = ((b*(-1))+Math.sqrt(d))/(2*a);
            System.out.println("x2: "+"\t"+ x2);
        }
    }
    // programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
    // i ekrana isvedam kvadratines lygties reiksmes intervale [-5, 5]
    public static void task4() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("kvadratines lygties ax*x+bx+c reiksmiu isvedimas intervale [-5, 5]");
        System.out.println("Iveskite skaiciu a = ");
        Double a = scanner.nextDouble();
        System.out.println("Iveskite skaiciu b = ");
        Double b = scanner.nextDouble();
        System.out.println("Iveskite skaiciu c = ");
        Double c = scanner.nextDouble();
        for (int i=-5; i<6; i++){
            System.out.println("reiksme "+i+ "rezultatas: "+((a*i*i)+(b*i)+c) );
        }
    }
//}


// programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
// i ekrana isvedam kvadratines lygties reiksmes intervale [-5, 5]
//    void task4() {
//
//    }

//    XTuple solveSquareEquation(Double a, Double b, Double c) {

//        Double bRoot = b * b;
//        Double d = bRoot - 4 * a * c;
//        XTuple result = new XTuple();
//        if (d >= 0) {
//            Double dRoot = Math.sqrt(d);
//            result.x1 = x(a, b, dRoot);
//            result.x2 = d == 0 ? null : x(a, b, -dRoot);
//        } else {
//            result.x1 = null;
//            result.x2 = null;
//        }
//        return result;
//    }
//
//    Double x(Double a, Double b, Double dRoot) {
//        return (-b + dRoot) / (2 * a);
//    }
//
//    class XTuple {
//        Double x1;
//        Double x2;
//    }
//}


}