import java.util.Scanner;

public class Main {

    Validator validator1=new Validator();
    public static void main(String[] args) throws Exception{
        Validator validator = new Validator();
        System.out.println("Pasirinkite uzduoti:");
        System.out.println("1 - Interaktyvus IBAN numerių tikrinimas");
        System.out.println("2 - IBAN numerių iš tekstinio failo tikrinimas");
        System.out.println("3 - Paleisti Http servisa");
        Scanner in = new Scanner(System.in);
        int choice = in.nextInt();
        switch(choice){
            case 1:
                System.out.println("Iveskite saskaitos numeri: ");
                validator.ibanCheck(in.next());
                break;
            case 2:
                System.out.println("iveskite kelia iki vailo bei jo pavadinima ");
                String url = in.next();
                validator.fileCheck(url);
                break;
            case 3:
                Http http = new Http();
                http.startService();
                break;
                default:
                    System.out.println("neteisingas pasirinkimas");
        }
    }



}
