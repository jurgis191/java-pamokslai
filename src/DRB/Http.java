import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Http {

    class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "This is the response";
            Integer responseStatus = 200;
            switch (t.getRequestMethod()) {
                case "GET":
                    String q = t.getRequestURI().getQuery();
                    String[] params = q.split("=|&");
                    Validator validator = new Validator();
                    response = "";
                    for(String p:params) {
                        if(!p.isEmpty())
                            response=response+p+validator.ibanCheck(p)+"\n";
                    }
                    break;

            }
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

     void startService ()throws Exception{
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/test", new MyHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }
}
