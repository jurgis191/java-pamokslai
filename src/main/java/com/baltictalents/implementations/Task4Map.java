package com.baltictalents.implementations;

import java.io.File;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task4Map {

public void readNumbers() throws Exception {
Map <String,Integer> flag = new HashMap<>();
        flag.put("Green", 0);
        flag.put("Yellow", 0);
        flag.put("Red", 0);

        String colorName="";
        Integer quantity=0;

        File file = new File("U1.txt");
        Scanner in = new Scanner(file);

        Integer numbersSize = in.nextInt();

        for (int i = 0; i < numbersSize; i++) {
            colorName = in.next();
            quantity = in.nextInt();
            if(colorName.equals("Z"))flag.put("Green", quantity);
            if(colorName.equals("R"))flag.put("Red", quantity);
            if(colorName.equals("G"))flag.put("Yellow", quantity);
        }
        in.close();

        Integer smallest = flag.get("Green");
        if (smallest > flag.get("Yellow")) smallest=flag.get("Yellow");
        if (smallest > flag.get("Red")) smallest=flag.get("Red");

        flag.put("Green", flag.get("Green")-smallest);
        flag.put("Yellow", flag.get("Yellow")-smallest);
        flag.put("Red", flag.get("Red")-smallest);

        PrintWriter writer = new PrintWriter( "R12.txt", "UTF-8");

                writer.write( smallest+ " ");
                writer.write(System.getProperty( "line.separator" ));
                writer.write( "G = "+flag.get("Yellow")+ " ");
                writer.write(System.getProperty( "line.separator" ));
                writer.write( "Z = " +flag.get("Green")+ " ");
                writer.write(System.getProperty( "line.separator" ));
                writer.write( "R = "+flag.get("Red")+ " ");

        writer.close();
    }
    }




