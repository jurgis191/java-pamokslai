package com.baltictalents.implementations;

import java.io.File;
import java.io.PrintWriter;
import java.util.*;

public class Task5 {


    public void readNumbers(String filename) throws Exception {
        File file = new File(filename);
        Scanner in = new Scanner(file);

        Integer ingredients = in.nextInt();
        Integer dishes = in.nextInt();
        Integer[] prices = new Integer[ingredients];

        Map<String, Integer> map = new HashMap<>();

        for (int i = 0; i < ingredients; i++){
            prices[i]=in.nextInt();
        }

        for (int i = 0; i < dishes; i++) {
            String stringColector="";
            Integer dishPrice=0;
            while (in.hasNextInt()==false){
                stringColector=stringColector+in.next()+" ";
            }

            int j=0;
            while ( j<ingredients){
                dishPrice = dishPrice + ( in.nextInt() * prices[j] );
                j++;
            }


            map.put(stringColector, dishPrice);


        }
        in.close();

        int sum = map.values().stream().mapToInt(Number::intValue).sum();

        List<String> keys = new ArrayList<>(map.keySet());





        PrintWriter writer = new PrintWriter( "R3.txt", "UTF-8");

        for(String dish: keys){
                writer.write( dish+ "\t\t" + map.get(dish));
            writer.write(System.getProperty( "line.separator" ));
         }
        writer.write( sum+ " ");
        writer.close();


    }

}
