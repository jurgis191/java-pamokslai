package com.baltictalents.implementations;

import java.io.File;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task6  {

    public void readFile () throws Exception {
        File file = new File("U4.txt");
        Scanner in = new Scanner(file);

        int sheepsQuantyti = in.nextInt();
        int dnrLenght = in.nextInt();
        int targetSheep = in.nextInt();
        targetSheep=targetSheep-1;

//        Map<String, String> sheeps = new HashMap<>();
//
//        for (int i=0; i<sheepsQuantyti; i++)
//            sheeps.put(in.next(),in.next());
//

        String sheeps [][] = new String[sheepsQuantyti][3];
        for (int i=0; i<sheepsQuantyti; i++) {
            sheeps[i][0] = in.next();
            sheeps[i][1] = in.next();
        }
        in.close();

        for (int j = 0; j<sheepsQuantyti; j++) {
            int matchCount = 0;
            for (int i = 0; i < dnrLenght; i++) {
                if(sheeps[targetSheep][1].charAt(i) == sheeps[j][1].charAt(i)&& j!=targetSheep) {
                    matchCount += 1;
            }
            sheeps[j][2] = String.valueOf(matchCount);
        }
            }

        PrintWriter writer = new PrintWriter( "R4.txt", "UTF-8");

        writer.write( sheeps[targetSheep][0]);
        writer.write(System.getProperty( "line.separator" ));

        Arrays.sort(sheeps, (a, b) -> Integer.compare(Integer.valueOf(b[2]), Integer.valueOf(a[2])));

        for (int i=0; i<sheepsQuantyti-1; i++) {
            writer.write(sheeps[i][0]+ " "+sheeps[i][2]);
            writer.write(System.getProperty("line.separator"));
        }
            writer.close();
    }
}
