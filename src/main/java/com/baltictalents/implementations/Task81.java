package com.baltictalents.implementations;

import java.util.ArrayList;
import java.util.List;

public class Task81 {

    public void test  () throws Exception{
        System.out.println("creating runable");
        List<Integer> list= new ArrayList<>();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                list.add(200);
            }
        };
        System.out.println("creating thread");
        Thread thread = new Thread(runnable);
        System.out.println("starting thread");
        thread.start();

        Thread.sleep(1000);
        System.out.println(list);
    }
}
