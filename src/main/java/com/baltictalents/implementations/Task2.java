package com.baltictalents.implementations;

public class Task2 {

        //    public static void main(String[] params) {
//        task1();
//        task2();
//        task3();
//        task4();
//    }
    /*
    Duoti trys skaiciai: a, b, c. Nustatykite ar sie skaiciai gali buti
    trikampio krastiniu ilgiai ir jei gali tai kokio trikampio:
    lygiakrascio, lygiasonio ar ivairiakrascio. Atspausdinkite
    atsakyma. Kaip pradinius duomenis panaudokite tokius skaicius:
    3, 4, 5
    2, 10, 8
    5, 6, 5
    5, 5, 5
     */
//    public void setValues(){
        public static int triangle1 [] =  {3, 4, 5};
        public static int triangle2 [] =  {2, 10, 8};
        public static int triangle3 [] =  {5, 6, 5};
        public static int triangle4 [] =  {5, 5, 5};
        //    }
        public void task1() {
            System.out.println(check(triangle1) ? "Trikampis egzistuoja":"trikampis neimanomas");
            System.out.println(check(triangle2) ? "Trikampis egzistuoja":"trikampis neimanomas");
            System.out.println(check(triangle3) ? "Trikampis egzistuoja":"trikampis neimanomas");
            System.out.println(check(triangle4) ? "Trikampis egzistuoja":"trikampis neimanomas");
        }
        boolean check (int [] tr ){
            Boolean rule1 = tr[0] + tr[1] > tr[2];
            Boolean rule2 = tr[1] + tr[2] > tr[0];
            Boolean rule3 = tr[2] + tr[0] > tr[1];
            Boolean result = rule1 && rule2 && rule3;
            return result;
        }
        /*
        Apskaiciuoti trikampiu plotus
        3, 4, 5
        2, 10, 8
        5, 6, 5
        5, 5, 5
         */
        public void task2(){
            System.out.println("Trikampio plotas "+plotas(triangle1));
            System.out.println("Trikampio plotas "+plotas(triangle2));
            System.out.println("Trikampio plotas "+plotas(triangle3));
            System.out.println("Trikampio plotas "+plotas(triangle4));
        }
        double plotas(int [] tr) {
            double p, S;
            p = (tr[0]+tr[1]+tr[2])/2;
            S=Math.sqrt(p*(p-tr[0])*(p-tr[1])*(p-tr[2]));
            return S;
        }

        // duotas sveikuju skaiciu masyvas {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15}
        // surasti min ir max
        // negalima naudoti min, max;
        public void task3() {
            int mas[] = {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15};
            System.out.println(maxi(mas)[0]);
            System.out.println(mini(mas)[0]);

        }
        int[] maxi(int [] mas){
            int temp [] = {mas[0], 0};
            for (int i=1; i<mas.length; i++){
                if(mas[i]>temp[0]){
                    temp[0]=mas[i];
                    temp[1]=i;}
            }
            return temp;
        }
        int[] mini(int [] mas){
            int temp [] = {mas[0], 0};
            for (int i=1; i<mas.length; i++){
                if(mas[i]<temp[0]){
                    temp[0]=mas[i];
                    temp[1]=i;}
            }
            return temp;
        }

        // duotas sveikuju skaiciu masyvas {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15}
        // isrikiuoti didejimo tvarka
        // negalima naudoti Collections.sort(array[Int]);
        public void task4() {
            int mas[] = {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15};
            int max = maxi(mas)[0];
            int maxIndex = maxi(mas)[1];
            int min = mini(mas)[0];
            int minIndex = mini(mas)[1];
            Integer temp1 [] = new Integer [mas.length];
            Integer temp8 [] = new Integer [mas.length];

            for(int i = 0 ; i < mas.length ; i++){
                int temp2 = mini(mas)[0];
                temp8[i]=temp2;
                int index = mini(mas)[1];
                mas[index]= max;
            }
            for(int j = 0; j<mas.length; j++)
                mas[j]=temp8[j];

            for(int i = 0 ; i < mas.length ; i++){
                int temp2 = maxi(mas)[0];
                temp1[i]=temp2;
                int index = maxi(mas)[1];
                mas[index]= min;
            }
            for(int j = 0; j<mas.length; j++)
                System.out.print(temp1[j] + "\t");
            System.out.println();
            for(int j = 0; j<mas.length; j++)
                System.out.print(temp8[j] + "\t");
            System.out.println();
        }

}
