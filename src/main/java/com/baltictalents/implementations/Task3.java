package com.baltictalents.implementations;
import java.math.BigDecimal;

public class Task3 {

    // uzpildyti sveikuju skaiciu masyva fibonaci seka. 1000 elementu.
    public void task1() {
        BigDecimal fibonaci[] = new BigDecimal [1000];
        BigDecimal fibo = new BigDecimal(0);
        fibonaci[0]=new BigDecimal(0);
        fibonaci[1]=new BigDecimal(1);
        for (int i = 2; i<1000; i++){
            fibonaci[i] = fibo.add(fibonaci[i-1]).add(fibonaci[i-2]);
        }
        for (int i = 0; i<1000; i++){
            System.out.println(fibonaci[i]);
        }
    }

    // parasyti funkcija kuri nustato ar sveikasis skaicius yra pirminis skaicius.
//    void task2() {

    boolean isPrime(int n) {
        Boolean res = true;
        if(n % 2 == 0) {
            return false;
        }
        else {
            int i = 3;
            while(i <= Math.sqrt(n) ) {
                if( (n % i) == 0) {
                    res=false;
                    return res;
                }
                i = i + 2;
            }
        }
        return res;
    }

    // rasti pirmus 100 pirminiu skaiciu ir uzpildyti jais masyva
    public void task3() {
        Integer primeArr [] = new Integer[100];
        primeArr [0]=1;
        primeArr [1]=2;
        primeArr [2]=3;
        int i=3;
        int j=4;

        while(i<100){
            if (isPrime(j)) {
                primeArr[i] = j;
                i++;
            }
            j++;
        }
        for(int h=0;h<100;h++) {
            System.out.println(primeArr[h] + "\t");
        }
    }

    // aprasyti 3 skirtingas klases kurios tarpusavyje turetu logiska sarysi
    void task4() {

    }
}
