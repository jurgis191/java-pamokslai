package com.baltictalents.implementations;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tasks7 {

    // testuojantis naudoti POSTMAN

    // sukurti http servisa, kuris turetu du endpointus:
    // 1. GET http://localhost:8080/currencies
    // 2. GET http://localhost:8080/currency/rates?currencyCode=USD
    // responsai turi buti grazinami json formatu.
    //  pvz: ["USD", "EUR", "GBP"]
    public void task1()throws Exception  {
        readLocal();
        save("USD_EUR", findcurency("USD_EUR"));
        save("USD_GBP", findcurency("USD_GBP"));
        save("USD_CHF", findcurency("USD_CHF"));
        save("USD_EUR", findcurency("USD_EUR"));
        save("USD_GBP", findcurency("USD_GBP"));
        save("USD_CHF", findcurency("USD_CHF"));
        save("USD_EUR", findcurency("USD_EUR"));
        save("USD_GBP", findcurency("USD_GBP"));
        save("USD_CHF", findcurency("USD_CHF"));
        save("USD_EUR", findcurency("USD_EUR"));
        save("USD_GBP", findcurency("USD_GBP"));
        save("USD_CHF", findcurency("USD_CHF"));

//        findcurency("USD_GBP");
//        findcurency("USD_CHF");
    }

    // valiutu kursus gauti is vieso API
    // kad per daznai nenaudoti vieso API del greitaveikos,
    // valiutu kursus teks laikinai saugoti atmintyje valanda laiko.
    void task2() {


    }

    private Map<String, Double> curency = new HashMap<>();

    private HttpServer howToStartHttpServer() throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
        server.createContext("/currencies", new MyHandler());
        server.createContext("/currency/rates", new CurrencyRateHandler());
        server.setExecutor(null); // creates a default executor

        return server;
    }

    private Double readExternal(String curency){
        return read ("https://free.currencyconverterapi.com/api/v6/convert?q="+curency+"&compact=y");
    }

    private void readLocal()throws Exception{
        HttpServer server = howToStartHttpServer();
        server.start();
        read ("http://localhost:8080/currencies");
        server.stop(0);
    }


    private Double read (String urlName) {
        String curencyName;
        if(urlName.length()>58)
            curencyName=urlName.substring(55,62);
        else
            curencyName = "local Server";

        try {
            String output;
            Double rezult;
            URL url = new URL(urlName);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();   //conn.setRequestMethod("GET");  conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));


//            System.out.println("\n"+curencyName);
//                while there if needed
            if (!curencyName.equals( "local Server")) {
                rezult=findDouble(br.readLine());
                output = String.valueOf(rezult);

            }else {
                output = br.readLine();
                rezult = 0.0;
                System.out.println("local Server call: "+output+"\n");
            }

            conn.disconnect();
            return rezult;

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return 0.0;
    }


    private Double findDouble (String str){
        Pattern p = Pattern.compile("(\\d+(?:\\.\\d+))");
        Matcher m = p.matcher(str);
        double a = 0.0;
        while(m.find()) {
            double d = Double.parseDouble(m.group(1));
            a=d;
        }

        return a;
    }


    void save (String name, Double val){
       curency.put(name, val);
       remove(name);

    }

    private void remove (String name){
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        curency.remove(name);
                    }
                },
                3600000
        );
    }

    private Double findcurency (String cur){
        Double result=0.0;
        if (curency.get(cur)!=null){
            System.out.print("internal value " + cur +": ");
            result = curency.get(cur);
            System.out.println(result);
            System.out.println();

        }
        else{
            result=readExternal(cur);
            System.out.print("external value " + cur +": ");
            System.out.println(result);
            System.out.println();
        }
        return result;
    }


    // http routes handler
    public static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "[\"USD : 1\", \"EUR : 2\", \"GBP : 3\"]";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
    public static class CurrencyRateHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "[\"USD\", \"EUR\", \"GBP\"]";
            t.sendResponseHeaders(200, response.length());
            t.getResponseBody();
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

}