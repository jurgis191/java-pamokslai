package com.baltictalents.implementations;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileFiguresReader implements FiguresReader {


    private String filename;

    public FileFiguresReader(String filename) {
        this.filename = filename;
    }

    @Override
    public List<FigureCount> read() throws Exception {
        File file = new File("resources/" + filename);
        Scanner in = new Scanner(file);

        Integer n = in.nextInt();

        List<FigureCount> figures = new ArrayList<>(Task62Impl.UNIQUE_FIGURES * n);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < Task62Impl.UNIQUE_FIGURES; j++) {
                int figureCount = in.nextInt();
                if (figureCount > 0) {
                    FigureCount figure = new FigureCount(j, figureCount);
                    figures.add(figure);
                }
            }
        }
        return figures;
    }
}
