package com.baltictalents.implementations;

import java.util.List;

public interface FiguresReader {
    List<FigureCount> read() throws Exception;
}
