package com.baltictalents.implementations;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task4 {

   public String[][] rezultList;

//////  Flag task
    public String[][] readNumbers(String filename) throws Exception {
        File file = new File(filename);
        Scanner in = new Scanner(file);

        Integer numbersSize = in.nextInt();
        String[][] flag = new String[numbersSize][2];

        for (int i = 0; i < numbersSize; i++) {
            flag[i][0] = in.next();
            flag[i][1] = in.next();
        }
        in.close();
        return flag;
    }

    public void writeNumbers(String [] numbers, String filename) throws Exception {
        PrintWriter writer = new PrintWriter( filename, "UTF-8");
        for (String number: numbers) {
            writer.write(number + " ");
            writer.write(System.getProperty( "line.separator" ));
        }
        writer.close();
    }
    public void writeNumbers2D(String [][] numbers, String filename) throws Exception {
        PrintWriter writer = new PrintWriter( filename, "UTF-8");
        for (String[] Row: numbers) {
            for ( String Col:Row){
            writer.write( Col+ " ");
            }
            writer.write(System.getProperty( "line.separator" ));
        }
        writer.close();
    }

    public void flagCount()throws Exception{
        String [][] mas = readNumbers("U1.txt");
        Integer size = mas.length;
        String[] rez = new String[4];
        Integer Z=0,R=0,G=0, smallest=0;
        for(int i=0; i<size;i++) {
            if (mas[i][0].equals("Z"))
                Z = Z + Integer.parseInt(mas[i][1]);
            if (mas[i][0].equals("R"))
                R = R + Integer.parseInt(mas[i][1]);
            if (mas[i][0].equals("G"))
                G = G + Integer.parseInt(mas[i][1]);
        }
        smallest = Z;
        if (smallest > R) smallest=R;
        if (smallest > G) smallest=G;

        Z=Z-smallest;
        R=R-smallest;
        G=G-smallest;

        rez[0]=String.valueOf(smallest);
        rez[1]="Z "+String.valueOf(Z);
        rez[2]= "R "+String.valueOf(R);
        rez[3]="G "+String.valueOf(G);

        System.out.println(rez[0]);
        writeNumbers(rez, "U1rez.txt");
    }
/////////end of flag task



//////Skiers task

    ///////
    public String[][] readSkiers(String filename) throws Exception {
        File file = new File(filename);
        Scanner in = new Scanner(file);

        Integer startNum = in.nextInt();
        String[][] startList = new String[startNum][4];

        for (int i = 0; i < startNum; i++) {
            String stringColector="";
            while (in.hasNextInt()==false){
                stringColector=stringColector+in.next()+" ";
            }
            startList[i][0] = stringColector;
            startList[i][1] = in.next();
            startList[i][2] = in.next();
            startList[i][3] = in.next();
        }

        Integer finishNum = in.nextInt();

        String[][] finishtList = new String[startNum][4];
        for (int i = 0; i < finishNum; i++) {
            String stringColector="";
            while (in.hasNextInt()==false){
                stringColector=stringColector+in.next()+" ";
            }
            finishtList[i][0] = stringColector;
            finishtList[i][1] = in.next();
            finishtList[i][2] = in.next();
            finishtList[i][3] = in.next();
        }
        in.close();
///////
        int i1=0;
        rezultList = new String[finishNum][4];
        for (int i = 0; i < startNum; i++)
            for (int j = 0; j < finishNum; j++)
                if (startList[i][0].equals(finishtList[j][0])) {
                    rezultList[i1][0] = startList[i][0];
                    int tmp = Integer.parseInt(finishtList[j][3]) - Integer.parseInt(startList[i][3]);
                    int m = 0;
                    if (tmp < 0) {
                        tmp = 60 + tmp;
                        m = -1;
                    }
                    rezultList[i1][3] = Integer.toString(tmp);
                    tmp = Integer.parseInt(finishtList[j][2]) - Integer.parseInt(startList[i][2]) + m;
                    m = 0;
                    if (tmp < 0) {
                        tmp = 60 + tmp;
                        m = -1;
                    }
                    rezultList[i1][2] = Integer.toString(tmp);
                    tmp = Integer.parseInt(finishtList[j][1]) - Integer.parseInt(startList[i][1]) + m;
                    m = 0;
                    if (tmp < 0) {
                        tmp = 60 + tmp;
                        m = -1;
                    }
                    rezultList[i1][1] = Integer.toString(tmp);
                    i1++; // except null strings in rezultList
                }

/// sorting array.
        int out1=0, out2=0, out3=0,out4=0, notequal=0;
        for(int s=0; s<4; s++) {
            out1=0; out2=0; out3=0;
            int i = 1;
            while  (i < rezultList.length && out3!=3 ) {
                int j = 1;
                out1=0; out2=0;
                while (j < rezultList[i].length && out2!=2) {
                        int k = 0;
                        int k1 = rezultList[i][j].length();
                        int k2 = rezultList[i - 1][j].length();
                        out1 = 0;
                        while (k < k1 && k < k2 && out1 != 1) {
                            if (rezultList[i][j].charAt(k) < rezultList[i - 1][j].charAt(k)) {
                                swap( i,i-1);
                                out1 = 1;
                                out2 = 2;
                                out3 = 3;
                                notequal=1;
                            }
                            if (rezultList[i][j].charAt(k) > rezultList[i - 1][j].charAt(k)) {
                                out1 = 1;
                                out2 = 2;
                                notequal=1;
                            }
                            k++;
                        }
                        j++;
                }
                if (notequal==0){
                    int k = 0;
                    int k1 = rezultList[i][0].length();
                    int k2 = rezultList[i - 1][0].length();
                    out4 = 0;
                    while (k < k1 && k < k2 && out4 != 1) {
                        if (rezultList[i][0].charAt(k) < rezultList[i - 1][0].charAt(k)) {
                            swap( i,i-1);
                            out4=1;
                        }
                        k++;
                    }
                }
                i++;
            }
            System.out.println();
        }
        return rezultList;
    }

    void swap(int x, int y){
            for (int j = 0; j < rezultList[y].length; j++) {
                String temp = String.valueOf(rezultList[y][j]);
                rezultList[y][j]=String.valueOf(rezultList[x][j]);
                rezultList[x][j]=String.valueOf(temp);
            }
        }

    public void skierCount()throws Exception{
        writeNumbers2D(readSkiers("U1.txt"),"U1rez.txt");

}}
