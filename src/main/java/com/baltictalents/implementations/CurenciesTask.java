package com.baltictalents.implementations;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CurenciesTask {
    public void task1()throws Exception  {
        readLocal();
        readExternal("USD_EUR");
        readExternal("USD_GBP");
        readExternal("USD_CHF");
    }
    void readExternal(String curency){
        read ("https://free.currencyconverterapi.com/api/v6/convert?q="+curency+"&compact=y");
    }

    void readLocal()throws Exception{
        HttpServer server = howToStartHttpServer();
        server.start();
        read ("http://localhost:8080/currencies");
        server.stop(0);
    }

    public void read (String urlName) {
        String curencyName;
        if(urlName.length()>58)
            curencyName=urlName.substring(55,62);
        else
            curencyName = "local Server";

        try {
            URL url = new URL(urlName);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();   //conn.setRequestMethod("GET");  conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("\n"+curencyName);
//                while ((
            if (curencyName!= "local Server")
                output = String.valueOf(findDouble(br.readLine()));
            else
                output = br.readLine();
//                ) != null) {
            System.out.println(output);
//                }
            conn.disconnect();

        } catch (
                MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
    }

    Double findDouble (String str){
        Pattern p = Pattern.compile("(\\d+(?:\\.\\d+))");
        Matcher m = p.matcher(str);
        double a = 0.0;
        while(m.find()) {
            double d = Double.parseDouble(m.group(1));
            a=d;
        }

        return a;
    }

    public HttpServer howToStartHttpServer() throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
        server.createContext("/currencies", new MyHandler());
        server.createContext("/currency/rates", new CurrencyRateHandler());
        server.setExecutor(null); // creates a default executor

        return server;
    }
    public static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "[\"USD : 1\", \"EUR : 2\", \"GBP : 3\"]";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
    public static class CurrencyRateHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "[\"USD\", \"EUR\", \"GBP\"]";
            t.sendResponseHeaders(200, response.length());
            t.getResponseBody();
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

}
