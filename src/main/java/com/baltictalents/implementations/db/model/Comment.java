package com.baltictalents.implementations.db.model;

public class Comment {

    private Integer id;
    private String name;
    private String email;
    private String summary;

    public Comment(Integer id, String name, String email, String summary) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.summary = summary;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getSummary() {
        return summary;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", summary='" + summary + '\'' +
                '}';
    }
}
