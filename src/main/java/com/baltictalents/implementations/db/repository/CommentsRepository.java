package com.baltictalents.implementations.db.repository;

import com.baltictalents.implementations.db.model.Comment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentsRepository {

    private Connection connection;

    private PreparedStatement findByIdQuery;

    public CommentsRepository(Connection connection) throws SQLException {
        this.connection = connection;
        this.findByIdQuery = connection
                .prepareStatement("select * from comments");
    }

    public List<Comment> find() throws SQLException {

        String query = "select * from comments";

        ResultSet resultSet = connection
                .prepareStatement(query)
                .executeQuery();

        List<Comment> comments = new ArrayList<>();
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String email = resultSet.getString("email");
            String summary = resultSet.getString("summary");
            comments.add(new Comment(id, name, email, summary));
        }
        return comments;
    }

    public List<Comment> findById(String findId) throws SQLException {

        findByIdQuery.setString(1, findId);
        ResultSet resultSet = findByIdQuery.executeQuery();

        List<Comment> comments = new ArrayList<>();
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("myuser");
            String email = resultSet.getString("email");
            String summary = resultSet.getString("summary");
            comments.add(new Comment(id, name, email, summary));
        }
        return comments;
    }

    public void save(Comment comment) throws SQLException {

        String query = "insert into comments " +
                "(id, name, email, summary) values (default, ?, ?, ?)";

        PreparedStatement preparedStatement = connection
                .prepareStatement(query);

        preparedStatement.setString(1, comment.getName());
        preparedStatement.setString(2, comment.getEmail());
        preparedStatement.setString(3, comment.getSummary());
        preparedStatement.execute();
    }
}
