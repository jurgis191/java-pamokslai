package com.baltictalents.tasks;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Tasks9 {

    // parasyti testus savo vienai vbe uzduociai, uzduotis turi susidaryti minimum is 2 funkciju.
    void task1() {

    }

    // Reikia naudoti ConcurrentHashMap kuris apsaugotas nuo multithread
    // arba naudoti synchronized ant bloku kur keiciam mapo reiksme.
    void task1Impl() throws Exception {

        //Map<Integer, Integer> integersCount = new ConcurrentHashMap<Integer, Integer>();
        Map<Integer, Integer> integersCount = new HashMap<Integer, Integer>();

        for (int i = 0; i < 100; i++) {
            integersCount.put(i, 0);
        }

        ExecutorService executor = Executors.newFixedThreadPool(10);

        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            executor.execute(() -> {

                for (int j = 0; j < 100; j++) {
                    final Integer newInteger = random.nextInt(10);

                    integersCount.computeIfPresent(newInteger, (k, v) -> v + 1);

//                    synchronized (integersCount.get(j)) {
//                        integersCount.computeIfPresent(newInteger, (k, v) -> v + 1);
//                    }
                }
            });
        }
        Thread.sleep(2000);

        Integer totalCount = 0;

        for (Integer k: integersCount.keySet()) {
            Integer count = integersCount.getOrDefault(k, 0);
            totalCount += count;
            System.out.println(k + " : " + count);
        }

        System.out.println("Total count: " + totalCount);
    }
}
