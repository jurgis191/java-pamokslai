package com.baltictalents.lessons;

import com.baltictalents.Main;
import com.baltictalents.lessons.work.Traveler;

import java.util.*;
import java.util.function.DoubleConsumer;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Lesson8 {

    // stream, lambda

    void lessonWork() {
        Traveler t1 = new Traveler("Andzej", 83, 'M');
        Traveler t2 = new Traveler("Eimantas", 83, 'M');
        Traveler t3 = new Traveler("Adomas", 25, 'M');
        Traveler t4 = new Traveler("Ieva", 25, 'F');
        Traveler t5 = new Traveler("Karolina", 23, 'F');
        Traveler t6 = new Traveler("Lavar", 35, 'M');
        Traveler[] travelers = new Traveler[] {t1, t2, t3, t4, t5, t6};

        List<Traveler> travelerList = new ArrayList<>(Arrays.asList(travelers));

        List<String> womenList = travelerList
                .stream()
                .filter(t -> t.getSex() == 'F')
                .sorted((tt1, tt2) -> Math.max(tt1.getAge(), tt2.getAge()))
                .map(t -> t.getName())
                .collect(Collectors.toList());

        Optional<Traveler> oldestTravelerOptional = travelerList
                .stream()
                .max(Comparator.comparing(Traveler::getAge));

        oldestTravelerOptional.ifPresent(t -> System.out.println(t.getName()));
        Traveler tt = oldestTravelerOptional.orElse(new Traveler("Admin", 18, 'M'));
        System.out.println(tt);


        Optional<String> sOptional = Optional.of("abc");
        sOptional
                .map(s -> s.toUpperCase())
                .ifPresent(s -> System.out.println(s));
    }

    void optionalMapFlatMap() {
        Optional<String> nameOptional = Optional.of("Lavar").map(name -> name.charAt(0) + "");
        Optional<String> lastnameOptional = Optional.of("Ball").map(last -> last.charAt(0) + "");

        Optional<String> initials = nameOptional.flatMap(n -> lastnameOptional.map(l -> n + l));

        initials.ifPresent(i -> System.out.println(i)); // LB
    }

    void selfImpementedMapAndFilter() {
        Traveler t1 = new Traveler("Andzej", 83, 'M');
        Traveler t2 = new Traveler("Eimantas", 83, 'M');
        Traveler t3 = new Traveler("Adomas", 25, 'M');
        Traveler t4 = new Traveler("Ieva", 25, 'F');
        Traveler t5 = new Traveler("Karolina", 23, 'F');
        Traveler t6 = new Traveler("Lavar", 35, 'M');
        Traveler[] travelers = new Traveler[]{t1, t2, t3, t4, t5, t6};

        List<Traveler> travelerList = new ArrayList<>(Arrays.asList(travelers));

        List<Traveler> mappedTravelers = map(travelerList, new Mapper() {
            @Override
            public Traveler apply(Traveler t) {
                return new Traveler(t.getName().toUpperCase(), t.getAge(), t.getSex());
            }
        });

        List<Traveler> filtredTravelers = filter(travelerList, new Filter() {
            @Override
            public Boolean predicate(Traveler traveler) {
                return traveler.getSex() == 'F';
            }
        });
        filtredTravelers.stream().forEach(t -> System.out.println(t));
    }

    static List<Traveler> map(List<Traveler> travelers, Mapper mapper) {
        List<Traveler> newTravelers = new ArrayList<>();
        for (Traveler t: travelers) {
            newTravelers.add(mapper.apply(t));
        }
        return newTravelers;
    }

    static List<Traveler> filter(List<Traveler> travelers, Filter filter) {
        List<Traveler> newTravelers = new ArrayList<>();
        for (Traveler t: travelers) {
            if (filter.predicate(t))
                newTravelers.add(t);
        }
        return newTravelers;
    }

    interface Filter {
        Boolean predicate(Traveler traveler);
    }

    interface Mapper {
        Traveler apply(Traveler t);
    }

    void lessonWorkPredicates() {
        DoubleConsumer doublePrinter = System.out::println;
        //IntPredicate isEven =

        DoubleStream
                .of(1, 2, 3, 4, 5, 6, 7)
                .map(Math::sqrt)
                .forEach(doublePrinter);
    }

    void predicateOrAnd() {

        System.out.println("abc".contains("a"));
        System.out.println("abc".contains("ab"));
        System.out.println("abc".contains("ac"));
        System.out.println("abc".contains("c"));
        System.out.println("abc".contains("abc"));

        Predicate<String> a = input -> input.contains("a");
        Predicate<String> b = input -> input.contains("b");

        Predicate<String> ab = a.and(b); // tas pats if (a.test("ab") && b.test("ab"))

        System.out.println("test ac:" + a.and(b).test("ac"));
        System.out.println("test bc:" + a.and(b).test("bc"));
        System.out.println("test ab:" + a.and(b).test("ab"));
    }

    void streamMapper() {
        Stream
                .iterate(0, i -> i + 1)
                .limit(100)
                .map(square())
                .forEach(System.out::println);
    }

    void collectToList() {
        List<Integer> list = Stream
                .iterate(0, i -> i + 1)
                .limit(1000)
                .collect(Collectors.toList());
    }

    static Function<Integer, Integer> square() {
        return i -> i * i;
    }

    static List<Integer> findEvenNumbers(Integer n, Predicate<Integer> predicate) {

        List<Integer> evenNumbers = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            if (predicate.test(i)) evenNumbers.add(i);
        }
        return evenNumbers;
    }

    static Predicate<Integer> isEvenLambda() {
        return i -> i % 2 == 0;
    }

    static Predicate<Integer> isEvenLamda2() {
        return i -> isEven(i);
    }

    static Predicate<Integer> isEvenLamda3() {
        return Lesson8::isEven;
    }

    static Boolean isEven(Integer i) {
        return i % 2 == 0;
    }

    static IntPredicate isPrimary() {
        return number -> IntStream
                .range(2, (int)Math.sqrt(number) + 1)
                .noneMatch(i -> number % i == 0);
    }
}
