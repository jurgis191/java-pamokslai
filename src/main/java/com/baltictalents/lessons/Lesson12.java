package com.baltictalents.lessons;

import com.baltictalents.lessons.db.model.Comment;
import com.baltictalents.lessons.db.model.User;
import com.baltictalents.lessons.db.model.UserCommentsCount;
import com.baltictalents.lessons.service.response.UserJsonSerializer;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Lesson12 {

    void jsonReaderWriter() throws Exception {
        User user = new User(1, "u1", "u1@gmail.com");

        Comment comment = new Comment(
                1,
                "www.delfi.lt",
                "s1",
                "c1",
                Optional.of(1),
                LocalDateTime.now()
        );

        // custom serializators for optional and localdatetime
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        String userJson = objectMapper.writeValueAsString(user);
        String commentJson = objectMapper.writeValueAsString(comment);

        System.out.println(commentJson);
        System.out.println(userJson);


        String userJsonString = "{\"id\":1,\"username\":\"u1\",\"email\":\"u1@gmail.com\"}";

        User userFromJson = objectMapper.readValue(userJsonString, User.class);

        System.out.println(userFromJson);

        System.out.println();

        Map<String, Object> jsonMap = objectMapper.readValue(userJsonString,
                new TypeReference<Map<String,Object>>(){});

        for (Map.Entry<String, Object> entry: jsonMap.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println(jsonMap.get("username"));

        String jsonList = "[1, 2, 3, 4, 5, 10, 12, 14]";
        List<Integer> listOfIntegers = objectMapper.readValue(jsonList,
                new TypeReference<List<Integer>>(){});

        for (Integer i: listOfIntegers) {
            System.out.println(i);
        }
    }

    static void jsonNode() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        SimpleModule module =
                new SimpleModule("UserJsonSerializer", new Version(1, 0, 0, null, null, null));
        module.addSerializer(User.class, new UserJsonSerializer());
        objectMapper.registerModule(module);

        String userCommentsCountJsonString = "{\"user\":{\"id\":1,\"name\":\"u1\",\"email\":\"u1@gmail.com\"},\"count\":10}";
        JsonNode userCommentsCountJsonNode = objectMapper.readTree(userCommentsCountJsonString);

        JsonNode userJsonNode = userCommentsCountJsonNode.get("user");

        Integer id = userJsonNode.get("id").asInt();
        String name = userJsonNode.get("name").asText();
        String email = userJsonNode.get("email").asText();

        Integer count = userCommentsCountJsonNode.get("count").asInt();

        User userFromJson = new User(id, name, email);

        UserCommentsCount userCommentsCount = new UserCommentsCount(userFromJson, count);
    }
}
