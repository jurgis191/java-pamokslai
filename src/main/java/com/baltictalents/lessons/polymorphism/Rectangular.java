package com.baltictalents.lessons.polymorphism;

public class Rectangular extends Square {

    public Double width;

    public Rectangular(Double height, Double width) {
        super(height);
        this.width = width;
    }

    @Override
    public Double area() {
        return getSideMeters() * width;
    }
}
