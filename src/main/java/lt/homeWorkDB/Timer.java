package lt.homeWorkDB;

import lt.homeWorkDB.Model.Activities;
import lt.homeWorkDB.Model.Entries;
import lt.homeWorkDB.repository.ActivitiesRepository;
import lt.homeWorkDB.repository.EntriesRepository;
import lt.homeWorkDB.repository.MysqlConnector;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Timer {
    /*

    functionality task:

    --- main functions ---

    ----------------------------------show started activities

    ----------------------------------finish activity

   -----------------------------------neded find time start by id

    ----------------------------------count duration start time and now

    ----------------------------------start new activity

    ---------------------------------- add activity to list

    ----------------------------------show activity list

    ----------------------------------remove activity from list

     */

    MysqlConnector mysqlConnector = new MysqlConnector();

    public void start()throws SQLException{

        System.out.println("Sveiki, programa Timer pasieido");

        Scanner scanner = new Scanner(System.in);

        System.out.println("Pasirinkite is meniu: ");
        System.out.println("1 - prideti veikla");
        System.out.println("2 - parodyti veiklu sarasa");
        System.out.println("3 - rasti veikla pagal id");
        System.out.println("4 - istrinti veikla pagal id");
        System.out.println("5 - pradeti veikla");
        System.out.println("6 - isvesti pradetas veiklas");
        System.out.println("7 - stabdyti veikla");


        Integer choise = scanner.nextInt();
        String id, duration;
        switch (choise) {
            case 1 :

                System.out.println("iveskite veiklos pavadinima");

                String name = scanner.next();

                System.out.println("iveskite veiklos aprasyma");

                String description = scanner.next();

                addActivity(name,description);

                break;

            case 2:

                showActivity ();

                break;

            case 3:

                System.out.println("iveskite id numeri");

                String findId = scanner.next();

                findByIdActivity(findId);

                break;

            case 4:

                System.out.println("iveskite id numeri");

                id = scanner.next();

                delete(id);

                break;

            case 5:

                System.out.println("imanomos veiklos");

                showActivity ();

                System.out.println("pasirinkite id numeri");

                id = scanner.next();

                startActivity(id);

                System.out.println("veikla pradeta");

                break;

            case 6:

                startedActivities();

                break;

            case 7:

                startedActivities();

                System.out.println("iveskite id");

                id = scanner.next();

                duration = calculateDuration(findStartTimeById(id));

                stopActivity(duration, id);

                break;
        }
    }
    private void addActivity  ( String name, String description)throws SQLException {
        Activities activities = new Activities(1, name, description,1);
        ActivitiesRepository activitiesRepository = new ActivitiesRepository(mysqlConnector.getConnection());
        activitiesRepository.save(activities);
    }
    private void showActivity ()throws SQLException{
        ActivitiesRepository activitiesRepository = new ActivitiesRepository(mysqlConnector.getConnection());
        activitiesRepository.find().forEach(System.out::println);
        //optional
    }
    private void findByIdActivity (String findId)throws SQLException{
        ActivitiesRepository activitiesRepository = new ActivitiesRepository(mysqlConnector.getConnection());
        activitiesRepository.findById(findId).forEach(System.out::println);
        //optional
    }
    private void delete (String findId)throws SQLException{
        ActivitiesRepository activitiesRepository = new ActivitiesRepository(mysqlConnector.getConnection());
        activitiesRepository.deleteActivity(findId);
    }

    private void startActivity (String id) throws SQLException {
        Entries entries = new Entries( 1, id, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), 0);
        EntriesRepository entriesRepository = new EntriesRepository(mysqlConnector.getConnection());
        entriesRepository.save(entries);
    }

    private void startedActivities () throws SQLException {
        ActivitiesRepository activitiesRepository = new ActivitiesRepository(mysqlConnector.getConnection());
        activitiesRepository.startedActivities().forEach(System.out::println);
    }

    private void stopActivity(String duration, String id) throws SQLException{
        EntriesRepository entriesRepository = new EntriesRepository(mysqlConnector.getConnection());
        entriesRepository.finishTask(duration, id);
    }

    private String findStartTimeById (String id) throws  SQLException {
        EntriesRepository entriesRepository = new EntriesRepository(mysqlConnector.getConnection());
        List<Entries> entry = new ArrayList<Entries>(entriesRepository.findById(id));
        return entry.get(0).getStartTime();
    }

    private String calculateDuration  (String starttime){
        String pattern = "yyyy-MM-dd HH:mm:ss";
        String curentTime = new SimpleDateFormat(pattern).format(Calendar.getInstance().getTime());
        Long diff = 0l;
        try {
            Date dateTime=new SimpleDateFormat(pattern).parse(starttime);
            Date now=new SimpleDateFormat(pattern).parse(curentTime);
            diff = now.getTime()-dateTime.getTime();
//            long diffSeconds = diff / 1000 % 60;
//            long diffMinutes = diff / (60 * 1000) % 60;
//            long diffHours = diff / (60 * 60 * 1000) % 24;
//            long diffDays = diff / (24 * 60 * 60 * 1000);
//
//            System.out.print(diffDays + " days, ");
//            System.out.print(diffHours + " hours, ");
//            System.out.print(diffMinutes + " minutes, ");
//            System.out.print(diffSeconds + " seconds.");
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        return String.valueOf(diff);
    }
}