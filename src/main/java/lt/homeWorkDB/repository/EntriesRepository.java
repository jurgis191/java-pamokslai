package lt.homeWorkDB.repository;

import lt.homeWorkDB.Model.Entries;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EntriesRepository {

    private Connection connection;

    private PreparedStatement findByIdQuery;

    private PreparedStatement startedActivity;

    private PreparedStatement updateQuery;

    public EntriesRepository(Connection connection) throws SQLException {
        this.connection = connection;
        this.findByIdQuery = connection
                .prepareStatement("select * from entries where id = ?");
        this.startedActivity = connection
                .prepareStatement("Select * from entries where duration = 0");
        this.updateQuery = connection
                .prepareStatement("UPDATE entries SET duration = ? WHERE id =?");
    }

   public void finishTask (String duration, String id) throws SQLException{

        updateQuery.setString(1, duration);
        updateQuery.setString(2, id);
        updateQuery.execute();
   }

    public List<Entries> findById(String findId) throws SQLException {

        findByIdQuery.setString(1, findId);
        ResultSet resultSet = findByIdQuery.executeQuery();

        List<Entries> entry = new ArrayList<>();
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            String activityId = resultSet.getString("activity_id");
            String startTime = resultSet.getString("start_time");
            Integer duration = resultSet.getInt("duration");
            entry.add(new Entries(id, activityId, startTime, duration));
        }
        return entry;
    }

    public void save(Entries entry) throws SQLException {

        String query = "insert into entries " +
                "(id, activity_id, start_time, duration) values (default, ?, ?, ?)";

        PreparedStatement preparedStatement = connection
                .prepareStatement(query);

        preparedStatement.setString(1, String.valueOf(entry.getActivityId()));
        preparedStatement.setString(2, String.valueOf(entry.getStartTime()));
        preparedStatement.setString(3, String.valueOf(entry.getDuration()));
        preparedStatement.execute();
    }
}
