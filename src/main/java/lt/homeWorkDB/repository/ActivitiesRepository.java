package lt.homeWorkDB.repository;

import com.mysql.cj.protocol.Resultset;
import lt.homeWorkDB.Model.Activities;
import lt.homeWorkDB.Model.StartedTasks;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ActivitiesRepository {


    private Connection connection;

    private PreparedStatement findByIdQuery;
    private PreparedStatement deleteQuery;
    private PreparedStatement startedActivitiesQuery;

    public ActivitiesRepository(Connection connection) throws SQLException {
        this.connection = connection;
        this.findByIdQuery = connection
                .prepareStatement(" select * from activities where id = ? ");
        this.deleteQuery = connection
                .prepareStatement("delete from activities where id = ?");
        this.startedActivitiesQuery = connection
                .prepareStatement("select e.id, a.name, e.start_time FROM entries e JOIN activities a WHERE e.activity_id = a.id AND e.duration = 0");
    }

    public List<Activities> find() throws SQLException {

        String query = "select * from activities";

        ResultSet resultSet = connection
                .prepareStatement(query)
                .executeQuery();

        List<Activities> activity = new ArrayList<>();
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String description = resultSet.getString("description");
            Integer user = resultSet.getInt("user");
            activity.add(new Activities(id, name, description, user));
        }

        return activity;
    }

    public List<Activities> findById(String findId) throws SQLException {

        findByIdQuery.setString(1, findId);
        ResultSet resultSet = findByIdQuery.executeQuery();

        List<Activities> activity = new ArrayList<>();
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String description = resultSet.getString("description");
            Integer user = resultSet.getInt("user");
            activity.add(new Activities(id, name, description, user));
        }

        return activity;
    }

    public void deleteActivity(String findId) throws SQLException {

        deleteQuery.setString(1, findId);
        deleteQuery.executeUpdate();

        }


    public List<StartedTasks> startedActivities () throws SQLException{

        ResultSet resultset = startedActivitiesQuery.executeQuery();

        List<StartedTasks> startedTask = new ArrayList<>();
        while(resultset.next()){
            String id = resultset.getString(1);
            String name = resultset.getString(2);
            String startedTime = resultset.getString(3);
            startedTask.add(new StartedTasks(id, name, startedTime));
        }
        return startedTask;
    }

    public void save(Activities activity) throws SQLException {

        String query = "insert into activities " +
                "(id, name, description, user) values (default, ?, ?, ?)";

        PreparedStatement preparedStatement = connection
                .prepareStatement(query);

        preparedStatement.setString(1, String.valueOf(activity.getName()));
        preparedStatement.setString(2, String.valueOf(activity.getDescription()));
        preparedStatement.setString(3, String.valueOf(activity.getUser()));
        preparedStatement.execute();
    }
}
