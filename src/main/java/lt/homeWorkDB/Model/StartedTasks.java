package lt.homeWorkDB.Model;

public class StartedTasks {

        String id;
        String name;
        String startTime;

        public StartedTasks( String id, String name, String startTime) {
                this.id = id;
                this.name = name;
                this.startTime = startTime;
        }

        @Override
        public String toString() {
                return "StartedTask " +
                        "id='" + id + '\'' +
                        "name='" + name + '\'' +
                        ", startTime='" + startTime ;
        }
}
