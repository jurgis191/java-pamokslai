package lt.homeWorkDB.Model;



public class Activities {
    private Integer id;
    private String name;
    private String description;
    private Integer user;

    public Activities(Integer id, String name, String description, Integer user) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return  "id=" + id +
                ", pavadinimas='" + name + '\'' +
                ", aprasymas='" + description + '\'' +
                ", vartotojo id=" + user
                ;
    }
}