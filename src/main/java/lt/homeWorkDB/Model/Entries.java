package lt.homeWorkDB.Model;

public class Entries {
    private Integer id;
    private String activityId;
    private String startTime;
    private Integer duration;

    public Entries(Integer id, String activityId, String startTime, Integer duration) {
        this.id = id;
        this.activityId = activityId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public Integer getId() {
        return id;
    }

    public String getActivityId() {
        return activityId;
    }

    public String getStartTime() {
        return startTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return
                "id=" + id +
                ", activityId='" + activityId + '\'' +
                ", startTime='" + startTime + '\'' +
                ", duration=" + duration
                ;
    }
}
